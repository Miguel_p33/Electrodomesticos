require "colorize"
require_relative '1ClasePadre'
require_relative '2ClaseTelevision'
require_relative '3ClaseComputador'

class CRUD
  
  def initialize 
    @productos_totales = []
    @contador_tv = 0
    @contador_compu = 0
    opcion=0
    @p_tv = 100
    @p_compu = 80
    @p_samsung = 40
    @p_lg = 20
    @p_modelo1 = 50
    @p_modelo2 = 100
    @p_negro = 20
    @p_gris = 10
    @total_pagar=0
    
    @paso=0
  end
  

  def cantidad_de_p (cantidad,productos_totales,contador_tv,contador_compu)
    
    while @cantidad > 0
      
      puts "Desea crear\n1)Telvisor  2)Computadora"
      x=gets.chomp.to_i
      system ("clear")
      
      if x == 1
        
        title = "televisor"
        creador_p = Telivition.new(title).create
        @contador_tv +=1
      elsif x == 2
        
        title = "computadora"
        creador_p = Computer.new(title).create
        @contador_compu +=1
      end
      
      @cantidad -=1
      @productos_totales << creador_p
    end
  end

  def sumatoria_t(for_,productos_totales)
   @sumatoria = 0
    for i in 0..@productos_totales.length-1 do
      
      @productos_totales[i-1].each do |calve,valor| (
        if valor == "televisor"
          @sumatoria += @p_tv
        elsif valor == "computadora"
          @sumatoria += @p_compu
        end

        if valor == "Samsung"
          @sumatoria += @p_samsung
        elsif valor == "LG"
          @sumatoria += @p_lg
        end
        
        if valor == "Plasma"
          @sumatoria += @p_modelo1
        elsif valor == "4K" 
          @sumatoria += @p_modelo2
        end
        
        if valor == "Negro"
          @sumatoria += @p_negro
        elsif valor == "Gris"
          @sumatoria += @p_gris
        end)
      end 
    end
    @sumatoria
  end

  def sumatoria_p(for_,productos_totales,x)
    @sumatoria = 0
    for i in x..x do
      
      @productos_totales[i-1].each do |calve,valor| (
        if valor == "televisor"
          @sumatoria += @p_tv
        elsif valor == "computadora"
          @sumatoria += @p_compu
        end

        if valor == "Samsung"
          @sumatoria += @p_samsung
        elsif valor == "LG"
          @sumatoria += @p_lg
        end
        
        if valor == "Plasma"
          @sumatoria += @p_modelo1
        elsif valor == "4K" 
          @sumatoria += @p_modelo2
        end
        
        if valor == "Negro"
          @sumatoria += @p_negro
        elsif valor == "Gris"
          @sumatoria += @p_gris
        end)
      end 
    end
    @sumatoria
  end
  
  def mostrar_c (for_,productos_totales)
    
    for i in 1..(@productos_totales.length) do
      puts "---------------#{i}------------------\n".colorize(:blue)
      @productos_totales[i-1].each do |calve,valor| (puts "#{calve} #{valor} ") 
      end
      precio_total = sumatoria_p(for_,productos_totales,i)
      
      puts "\nPrecio del producto #{precio_total}"; puts
    end
  end
  
  def contador_p(contador_tv,contador_compu)
  
    if @contador_tv > 0
      puts "Su total de televisores es #{@contador_tv}".colorize(:blue)
    end
    
    if @contador_compu > 0
      puts "Su total de computadoras es #{@contador_compu}".colorize(:blue)
    end
    
    puts "Su total a pagar es #{sumatoria_t(@for_,@productos_totales)}".colorize(:blue)
  end

  def opcion_e(opcion,for_,productos_totales)
    
    mostrar_c(@for_,@productos_totales)
    
    puts "Que producto desea editar:".colorize(:yellow)
    edit_p=gets.chomp.to_i-1
    system("clear")
    
    edit=@productos_totales[edit_p]
    
    i=1
    queEs = ""
    edit.each do |clave,valor| ( 
      puts "#{i}".colorize(:blue) + " #{clave} #{valor}"
      if valor == "televisor"
        queEs = valor
      elsif valor == "computadora"
        queEs = valor
      end
      
      i+=1) 
    end
    
    
    puts "\nQue desea editarle a este producto".colorize(:yellow)
    edit_c=gets.chomp.to_i-1
    
    if queEs == "televisor"
      case edit_c
        
      when 0 then system("clear");puts "\nNo puedes cambiar el tipo del producto\n".colorize(:red)
      when 1 then edit[:marca] = Telivition.new("").update(edit_c)
      when 2 then edit[:modelo] = Telivition.new("").update(edit_c)
      when 3 then edit[:color] = Telivition.new("").update(edit_c)      
        @productos_totales[edit_p] = edit
      end
    end
    
    if queEs == "computadora"
      case edit_c
        
      when 0 then puts "No puedes cambiar el tipo del producto"
      when 1 then edit[:marca] = Computer.new("").update(edit_c)
      when 2 then edit[:modelo] = Computer.new("").update(edit_c)
      when 3 then edit[:color] = Computer.new("").update(edit_c)
        @productos_totales[edit_p] = edit
      end
    end
  end
  
  def opcion_b(opcion,for_,productos_totales)
    mostrar_c(@for_,@productos_totales)
    
    puts "Que producto desea borrar ".colorize(:red)
    edit_p=gets.chomp.to_i-1
    system ("clear")
    
    @productos_totales.delete_at(edit_p)
  end

  def update_precio(act_precio)
    system("clear")
    case @act_precio
    when 1 then print "Ingrese nuevo precio del Televisor: ".colorize(:green) ; @cambio=gets.chomp.to_i ; @p_tv = @cambio
    when 2 then print "Ingrese nuevo precio de la Computadora: ".colorize(:green) ; @cambio=gets.chomp.to_i ; @p_compu = @cambio
    when 3 then print "Ingrese nuevo precio de la marca Samsung: ".colorize(:green) ; @cambio=gets.chomp.to_i ; @p_samsung = @cambio
    when 4 then print "Ingrese nuevo precio de la marca LG: ".colorize(:green) ; @cambio=gets.chomp.to_i ; @p_lg = @cambio
    when 5 then print "Ingrese nuevo precio del Modelo1: ".colorize(:green) ; @cambio=gets.chomp.to_i ; @p_modelo1 = @cambio
    when 6 then print "Ingrese nuevo precio del Modelo2: ".colorize(:green) ; @cambio=gets.chomp.to_i ; @p_modelo2 = @cambio
    when 7 then print "Ingrese nuevo precio del color Negro: ".colorize(:green) ; @cambio=gets.chomp.to_i ; @p_negro = @cambio
    when 8 then print "Ingrese nuevo precio del color Gris: ".colorize(:green) ; @cambio=gets.chomp.to_i ; @p_gris = @cambio
    end
  end

  def mostar_precio_con
    system("clear")    
    puts "Lista de precios\n".colorize(:blue)
    puts "Productos:".colorize(:green)
    puts "1) Televisor = #{@p_tv}\n2) Computadora = #{@p_compu}"
    puts "\nMarca:".colorize(:green)
    puts "3) Samsung = #{@p_samsung}\n4) LG = #{@p_lg}"
    puts "\nModelo".colorize(:green)
    puts "5) Modelo1 = #{@p_modelo1}\n6) Modelo2 = #{@p_modelo2}"
    puts "\nColor".colorize(:green)
    puts "7) Negro = #{@p_negro}\n8) Gris = #{@p_gris}"
  end

  def mostar_precio_sin
    system("clear")    
    puts "Lista de precios\n".colorize(:blue)
    puts "Productos:".colorize(:green)
    puts "Televisor = #{@p_tv}\nComputadora = #{@p_compu}"
    puts "\nMarca:".colorize(:green)
    puts "Samsung = #{@p_samsung}\nLG = #{@p_lg}"
    puts "\nModelo".colorize(:green)
    puts "Modelo1 = #{@p_modelo1}\nModelo2 = #{@p_modelo2}"
    puts "\nColor".colorize(:green)
    puts "Negro = #{@p_negro}\nGris = #{@p_gris}"
  end

  def opcion_confi()
    puts "Entro a la configuracion de precios".colorize(:blue)
    @act_precio = 0
    while @act_precio < 2
      system("clear")
      mostar_precio_sin
      puts "\nQue desea hacer:".colorize(:yellow)
      puts "1)Actualizar  ".colorize(:green) + "2)Salir".colorize(:red)
      @act_precio=gets.chomp.to_i
      system("clear")
      if @act_precio == 1
        mostar_precio_con
        puts "\nQue precio desea modificar:".colorize(:yellow)
        @act_precio=gets.chomp.to_i
        update_precio(@act_precio)
      end
    
    end
  end
  
  def main
    opcion = 0
    while opcion < 5
      system("clear")
      puts "Que desea hacer:".colorize(:yellow)
      puts "1)Crear  ".colorize(:green) + "2)Editar  ".colorize(:yellow) + "3)Borrar  ".colorize(:red) + "4)Configuracion  ".colorize(:cyan) + "5)Salir"
      opcion=gets.chomp.to_i
      
      if opcion == 1
        system("clear")
        puts "Cuantos productos desea crear:"
        @cantidad = gets.chomp.to_i
        @for_= @cantidad
        system("clear")
        cantidad_de_p(@cantidad,@productos_totales,@contador_tv,@contador_compu)
        
      elsif opcion == 2
        puts "Desea editar "
        system ("clear")
        opcion_e(opcion,@for_,@productos_totales)
      elsif opcion == 3
        system ("clear")
        opcion_b(@opcion,@for_,@productos_totales)
      elsif opcion ==4
        system ("clear")
        opcion_confi
      end
      
      mostrar_c(@for_,@productos_totales)
    end
    system ("clear")
    
    
    
    mostrar_c(@for_,@productos_totales)
    
    
    contador_p(@contador_tv,@contador_compu)
    puts
  end
  

end
system("clear")
abcd=CRUD.new
abcd.main
