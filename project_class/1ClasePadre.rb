class Line_white
  attr_accessor :producto, :marca, :modelo, :color, :consumo_e, :peso, :resolucion

  def initialize(title)
    @title = title
  end

  def create

    puts "Que marca de #{@title} desea\n1)Samsung   2)LG"
    marca_p=gets.chomp.to_i
    system ("clear")
    case marca_p
      when 1 then @marca = "Samsung"
      when 2 then @marca = "LG"
    end

    puts "De que color desea su #{@title}\n1)Negro  2)Gris"
    color_p=gets.chomp.to_i
    system ("clear")
    case color_p
      when 1 then @color = "Negro"
      when 2 then @color = "Gris"
    end
  end

  def update(edit_c)
    system("clear")
    case edit_c
    when 1 then
      puts "Que marca desea\n1)Samsung   2)LG"
      marca_p=gets.chomp.to_i
      system ("clear")
  
      case marca_p
      
        when 1 then @marca = "Samsung"
        when 2 then @marca = "LG"		
      end
    
    when 3 then
      puts "Que color desea\n1)Negro   2)Gris"
      color_p=gets.chomp.to_i
      system ("clear")
      
      case color_p
        when 1 then @color = "Negro"
        when 2 then @color = "Gris"
      end
    end
  end
end